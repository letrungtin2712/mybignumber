public class MyBigNumber {

    public  String sum(String st1, String st2){
        int num1, num2;
        // Assign short and long number
        String shorterStr = shorterStn(st1,st2);
        String longerStr = longerStn(st1,st2);
        String result = "";
        // Adding 0 in front of shorter number
        int count = Math.abs(st1.length() - st2.length());
        if (count > 0){
            for (int index = 0; index < count; index++){
                shorterStr = "0" + shorterStr;
            }
        }
        int i = shorterStr.length()-1;
        int j = longerStr.length()-1;
        int temp1 = 0 ; // Get last digit
        int temp2 = 0; // Get the remainder

        System.out.println(longerStr);
        System.out.println("+");
        System.out.println(shorterStr);
        System.out.println("------------");

        System.out.println("The result is: ");

        // Get each digit of a number than add it together
        while (i >=0){
            num1 = Integer.parseInt(String.valueOf(shorterStr.charAt(i)));
            num2 = Integer.parseInt(String.valueOf(longerStr.charAt(j)));
            // The total will be the sum of num1 + num2 + the remainder
            int total = num1 + num2 + temp2;
            //if the total is more than 10, update the temp1 and temp2
            if (total >= 10){
                temp1 = total % 10; //
                temp2 = (total/10) % 10;
            }else {
                temp1 = total;
                temp2 = 0;
            }
            System.out.println("The total is the sum of (" + num1+", " + num2+", " + temp2 +"): "  + total);
            System.out.println("Remember: " + temp2);
            result =  temp1+result ;
            System.out.println("The current calculation: " + result);
            System.out.println("***********************");

            i= i-1;
            j=j-1;
        }

        return result;
    };

    //Get shorter number
    public  String shorterStn(String num1, String num2){
        if (num1.length() < num2.length()) return num1;
        return num2;
    }
    //Get longer number
    public  String longerStn(String num1, String num2){
        if (num2.length() > num1.length()) return num2;
        return num1;
    }
}
