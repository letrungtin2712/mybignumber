import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyBigNumberTest {
    @Test
    void shorterNumber(){
        var bigNumber = new MyBigNumber();
        assertEquals("123",bigNumber.shorterStn("123","2090"));
    }
    @Test
    void longerNumber(){
        var bigNumber = new MyBigNumber();
        assertEquals("2090",bigNumber.longerStn("123","2090"));
    }

    @Test
    void sum(){
        var bigNumber = new MyBigNumber();
        assertEquals("4213", bigNumber.sum("123","4090"));
    }

}